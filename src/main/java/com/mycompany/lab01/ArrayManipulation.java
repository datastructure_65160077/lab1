/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab01;

/**
 *
 * @author pasinee
 */
public class ArrayManipulation {

    public static void main(String[] args) {
        int[] numbers = {5, 8, 3, 2, 7};
        String[] names = {"Alice", "Bob", "Charlie", "David"};
        double[] values = new double[4];
    
        // 3. Print the elements of the "numbers" array using a for loop.
        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i] + " ");
        }
        System.out.println();

        // 4. Print elements of the "names" array using a for-each loop
        for (String name : names) {
            System.out.print(name + " ");
        }
        System.out.println();

        // 5. Initialize the elements of the "values" array with any four decimal values
        values[0] = 1.1;
        values[1] = 2.2;
        values[2] = 3.3;
        values[3] = 4.4;

        // 6. Calculate and print the sum of all elements in the "numbers" array
        int sum = 0;
        for (int num : numbers) {
            sum += num;
        }
        System.out.println("Sum of elements in the 'numbers' array : " + sum);

        // 7. Find and print the maximum value in the "values" array
        double max = Double.MIN_VALUE;
        for (double value : values) {
            if (value > max) {
                max = value;
            }
        }
        System.out.println("Maximum value in the 'values' array : " + max);

        // 8. Create a new string array "reversedNames" 
        String[] reversedNames = new String[names.length];
        for (int i = 0; i < names.length; i++) {
            reversedNames[i] = names[names.length - 1 - i];
        }

        for (String reversedName : reversedNames) {
            System.out.print(reversedName + " ");
        }
        System.out.println();
        
        // BONUS
        for (int i = 0; i < numbers.length - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < numbers.length; j++) {
                if (numbers[j] < numbers[minIndex]) {
                    minIndex = j;
                }
            }
            int temp = numbers[i];
            numbers[i] = numbers[minIndex];
            numbers[minIndex] = temp;
        }

        for (int num : numbers) {
            System.out.print(num + " ");
        }
        System.out.println();

        
    }
}
