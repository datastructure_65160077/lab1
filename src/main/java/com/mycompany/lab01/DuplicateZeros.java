/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab01;

/**
 *
 * @author pasinee
 */
public class DuplicateZeros {
    public static void main(String[] args) {
        int[] arr = {1, 0, 8, 3, 0, 4, 5, 0};

        System.out.println("Original array:");
        printArray(arr);

        duplicateZeros(arr);

        System.out.println("Array after duplicating zeros:");
        printArray(arr);
    }

    public static void duplicateZeros(int[] arr) {
        int n = arr.length;
        int zerosCount = 0;

        // Count zeros 
        for (int num : arr) {
            if (num == 0) {
                zerosCount++;
            }
        }

        int originalIndex = n - 1;
        int modifiedIndex = n + zerosCount - 1;

        while (originalIndex >= 0) {
            if (arr[originalIndex] == 0) {
                if (modifiedIndex < n) {
                    arr[modifiedIndex] = 0;
                }
                modifiedIndex--;
            }

            if (modifiedIndex < n) {
                arr[modifiedIndex] = arr[originalIndex];
            }
            originalIndex--;
            modifiedIndex--;
        }
    }

    public static void printArray(int[] arr) {
        for (int num : arr) {
            System.out.print(num + " ");
        }
        System.out.println();
    }
}

